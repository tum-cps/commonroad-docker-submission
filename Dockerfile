FROM gitlab.lrz.de:5005/tum-cps/commonroad-docker-submission/base:crcomp24

RUN git clone https://gitlab.lrz.de/tum-cps/commonroad-interactive-scenarios.git && \
    pip install -r ./commonroad-interactive-scenarios/requirements.txt

# Copy the local planner directory into the Docker image
COPY ./planner /commonroad/planner
# Copy the requirements file for the planner and install its dependencies
COPY ./planner/commonroad_rp/requirements.txt /commonroad/planner/commonroad_rp/requirements.txt
RUN pip install -r /commonroad/planner/commonroad_rp/requirements.txt

ENV PYTHONPATH="/commonroad-interactive-scenarios/:${PYTHONPATH}"

COPY ./simulation /commonroad-interactive-scenarios/simulation

ENTRYPOINT python3 /commonroad/planner
