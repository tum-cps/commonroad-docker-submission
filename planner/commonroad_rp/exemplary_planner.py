import os
import sys
import copy
import numpy as np
from math import sin, cos
# CommonRoad
from commonroad.planning.planning_problem import PlanningProblem
from commonroad.scenario.scenario import Scenario
from commonroad.scenario.state import InitialState, CustomState
from commonroad_route_planner.route_planner import RoutePlanner
# reactive planner
from commonroad_rp.reactive_planner import ReactivePlanner
from commonroad_rp.utility.config import ReactivePlannerConfiguration
from commonroad_rp.utility.utils_coordinate_system import CoordinateSystem


def simulate_with_exemplary_planner(current_scenario: Scenario,
                                    state_current_ego: [InitialState, CustomState], planning_problem: PlanningProblem):
    # configuration
    config_name = "default"
    config = ReactivePlannerConfiguration.load(f"/commonroad/planner/commonroad_rp/configurations/{config_name}.yaml")
    config.update(current_scenario, planning_problem=planning_problem)
    # path planning
    route_planner = RoutePlanner(config.scenario.lanelet_network, config.planning_problem)
    route = route_planner.plan_routes().retrieve_first_route()
    reference_path = route.reference_path
    reference_lanelet = current_scenario.lanelet_network.find_lanelet_by_id(route.lanelet_ids[-1])
    while reference_lanelet.successor:
        lanelet_id = reference_lanelet.successor[0]
        reference_lanelet = current_scenario.lanelet_network.find_lanelet_by_id(lanelet_id)
        reference_path = np.vstack((reference_path, reference_lanelet.center_vertices[1:]))
    # define the reactive planner
    planner = ReactivePlanner(config)
    planner.x_0 = state_current_ego
    if not hasattr(planner.x_0, 'steering_angle'):
        planner.x_0.steering_angle = 0
    if not hasattr(planner.x_0, 'yaw_rate'):
        planner.x_0.yaw_rate = 0
    planner.set_reference_path(reference_path)
    planner.set_desired_velocity(current_speed=planner.x_0.velocity)
    sys.stderr = open(os.devnull, 'w')
    try:
        planner.reset(config, collision_checker=planner.collision_checker,
                      coordinate_system=CoordinateSystem(reference_path))
        # trajectory planning
        optimal = planner.plan()
        next_state = optimal[0].state_list[1]

    except:
        # decelerate to full stop if the reactive planner failed
        next_state = copy.deepcopy(state_current_ego)
        next_state.steering_angle = 0.0
        a = -2.0
        dt = 0.1
        if next_state.velocity > 0:
            v = next_state.velocity
            x, y = next_state.position
            o = next_state.orientation
            next_state.position = np.array([x + v * cos(o) * dt, y + v * sin(o) * dt])
            next_state.velocity += a * dt
    sys.stderr = sys.__stderr__
    return next_state